" plugins 
	filetype plugin indent on
	
	call plug#begin()
		" theme
		Plug 'sickill/vim-monokai'
	call plug#end()

" files
	set noswapfile
	set autochdir

" view
	set number
	set cursorline
	syntax on
	colorscheme monokai
	set t_Co=256

" identation
	set listchars=tab:\|\ 
	set list
	set tabstop=2
	set shiftwidth=2
	set autoindent

" splits/panels
	map <C-Y> <C-W><C-h>
	map <C-U> <C-W><C-j>
	map <C-I> <C-W><C-k>
	map <C-O> <C-W><C-l>

	set splitbelow
	set splitright

" fuzzy search
	set wildmenu


" search
	set ignorecase
	set smartcase
	set incsearch
	set hlsearch

" tabs
	nnoremap <S-t> :tabnew<CR>
	map <S-w> :bp <BAR> bd #<CR>

	nnoremap q :q<CR>
	nnoremap Y gT
	nnoremap O gt

" escape
	nnoremap <C-c> <Esc>
	inoremap <C-c> <Esc>

	" fixes slow Esc
	set timeout timeoutlen=75 ttimeoutlen=10

" fold
	nnoremap <space> za
	vnoremap <space> zf

" cursor
	let &t_ti.="\e[1 q"

" move inside wrapped lines 
	nnoremap j gj
	nnoremap k gk

" move faster
	" up and down 5 times
		nmap J 5j
		nmap K 5k
	
	" move by word
		nmap H b
		nmap L w
	
	" beginning and end of line
		nmap <c-h> _
		nmap <c-l> $

	" beginning and end of file
		nmap <c-k> gg
		nmap <c-j> G

" close faster
	inoremap <c-w> <Esc>:wq<CR>
	map <c-w> :wq<CR>

" utf8
	set encoding=utf-8
	set fileencoding=utf-8
